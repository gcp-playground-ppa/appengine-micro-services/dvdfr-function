import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { Request, Response } from 'express'
import * as parser from "xml2json";

class Dvd {
    public dvdfr: number;
    public ean: string;
    public title: string;
    public year: string;
    public cover: string;

    public constructor(dvdfrid: number, ean: string, title: string, year: string, cover: string) {
        this.dvdfr = dvdfrid;
        this.ean = ean;
        this.title = title;
        this.year = year;
        this.cover = cover;
    }
}

const fetchDvdForEan = async (ean: string) => {
    const response = await axios.get(`https://www.dvdfr.com/api/search.php?gencode=${ean}`);
    return await response.data;
}

export async function dvdfr(req: Request, res: Response) {

    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'GET');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    } else {
        await doFetchDvdFr(req, res);
    }

}

async function doFetchDvdFr(req: Request, res: Response) {
    const dvds: Dvd[] = [];
    const ean = req.query.ean as string;
    try {
        const xml = await fetchDvdForEan(ean);
        const json = parser.toJson(xml);
        const frdvds = JSON.parse(json);
        pushDvdWhenTitleExists(frdvds, frdvds.dvds.dvd.titres.fr, ean, dvds);
        pushDvdWhenTitleExists(frdvds, frdvds.dvds.dvd.titres.vo, ean, dvds);
        pushDvdWhenTitleExists(frdvds, frdvds.dvds.dvd.titres.alternatif, ean, dvds);
        res.status(200);
        res.send(JSON.stringify(dvds));
    }
    catch (err) {
        res.status(500);
        res.send(err);
    }
}

function pushDvdWhenTitleExists(xml: any, titre: any, ean: string, dvds: Dvd[]) {
    if (titre !== "" && JSON.stringify(titre) !== "{}") {
        dvds.push(new Dvd(xml.dvds.dvd.id, ean, titre, xml.dvds.dvd.annee, xml.dvds.dvd.cover))
    }
}
