import chai = require('chai')
import chaiHttp = require('chai-http')
import * as sinon from "sinon";
import axios from "axios";
import * as express from "express"

import { dvdfr } from '../src/index'

const app = express()
app.get('/', dvdfr)

chai.use(chaiHttp)
const expect = chai.expect

const xml = "<dvds generator=\"$Id: dvd.tpl 855 2008-08-04 15:53:24Z glapierre $\">"
            +"<dvd>"
            +"<id>12996</id>"
            +"<media>DVD</media>"
            +"<cover>"
            +"https://www.dvdfr.com/images/dvd/covers/200x280/7282651ae9b72d7e0d8d4d1afdac4014/12996/3d-alien_quadrilogy_1_simple.0.jpg"
            +"</cover>"
            +"<titres>"
            +"<fr>Alien</fr>"
            +"<vo/>"
            +"<alternatif>Alien, le 8ème passager</alternatif>"
            +"<alternatif_vo/>"
            +"</titres>"
            +"<annee>1979</annee>"
            +"<edition>Édition Simple</edition>"
            +"<editeur>20th Century Studios</editeur>"
            +"<stars>"
            +"<star type=\"Réalisateur\" id=\"2039\">Ridley Scott</star>"
            +"</stars>"
            +"</dvd>"
            +"</dvds>";

const json = "[{\"dvdfr\":\"12996\",\"ean\":\"3344428015602\",\"title\":\"Alien\",\"year\":\"1979\",\"cover\":\"https://www.dvdfr.com/images/dvd/covers/200x280/7282651ae9b72d7e0d8d4d1afdac4014/12996/3d-alien_quadrilogy_1_simple.0.jpg\"},{\"dvdfr\":\"12996\",\"ean\":\"3344428015602\",\"title\":\"Alien, le 8ème passager\",\"year\":\"1979\",\"cover\":\"https://www.dvdfr.com/images/dvd/covers/200x280/7282651ae9b72d7e0d8d4d1afdac4014/12996/3d-alien_quadrilogy_1_simple.0.jpg\"}]"

describe('Hello function', () => {
    let stub: sinon.SinonStub;

    beforeEach(() => {
      stub = sinon.stub(axios, "get");
      stub.resolves({status: 200, data: xml})
    });

    afterEach(() => {
      stub.restore();
    });

    it("should fetch DVDFR URL", (done) => {
        chai.request(app)
            .get('/?ean=1234')
            .end((err, res) => {
                expect(
                  stub.calledWith("https://www.dvdfr.com/api/search.php?gencode=1234")
                ).to.be.true;
                done()
            })
    });

    it("return correct JSON", (done) => {
      chai.request(app)
          .get('/?ean=3344428015602')
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.text).to.be.equal(json);
            done()
          });
    });
/*
    it('Get 200 response', function (done) {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                expect(err).to.be.null
                expect(res.text).to.be.equal('{}')
                expect(res).to.have.status(200)
                done()
            })
    })*/
})