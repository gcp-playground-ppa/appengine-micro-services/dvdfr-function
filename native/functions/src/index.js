"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dvdfr = void 0;
const axios_1 = require("axios");
const parser = require("xml2json");
class Dvd {
    constructor(dvdfrid, ean, title, year, cover) {
        this.dvdfr = dvdfrid;
        this.ean = ean;
        this.title = title;
        this.year = year;
        this.cover = cover;
    }
}
const fetchDvdForEan = async (ean) => {
    const response = await axios_1.default.get(`https://www.dvdfr.com/api/search.php?gencode=${ean}`);
    return await response.data;
};
async function dvdfr(req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    if (req.method === 'OPTIONS') {
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
    }
    else {
        await doFetchDvdFr(req, res);
    }
}
exports.dvdfr = dvdfr;
async function doFetchDvdFr(req, res) {
    const dvds = [];
    const ean = req.query.ean;
    try {
        const xml = await fetchDvdForEan(ean);
        const json = parser.toJson(xml);
        const frdvds = JSON.parse(json);
        pushDvdWhenTitleExists(frdvds, frdvds.dvds.dvd.titres.fr, ean, dvds);
        pushDvdWhenTitleExists(frdvds, frdvds.dvds.dvd.titres.vo, ean, dvds);
        pushDvdWhenTitleExists(frdvds, frdvds.dvds.dvd.titres.alternatif, ean, dvds);
        res.status(200);
        res.send(JSON.stringify(dvds));
    }
    catch (err) {
        res.status(500);
        res.send(err);
    }
}
function pushDvdWhenTitleExists(xml, titre, ean, dvds) {
    if (titre !== "" && JSON.stringify(titre) !== "{}") {
        dvds.push(new Dvd(xml.dvds.dvd.id, ean, titre, xml.dvds.dvd.annee, xml.dvds.dvd.cover));
    }
}
