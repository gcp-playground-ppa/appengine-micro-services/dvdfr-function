import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as parser from "xml2json";

const request = async (ean: string) => {
    const response = await axios.get(`https://www.dvdfr.com/api/search.php?gencode=${ean}`);
    return response.data;
}

const dvdfr = new gcp.cloudfunctions.HttpCallbackFunction("dvdfr", async (req, res) => {
    const dvds: any[] = [];
    if(req.query.ean) {
        const ean = req.query.ean as string;
        const xml = await request(ean);
        const json = parser.toJson(xml);
        const frdvds = JSON.parse(json);
        if(frdvds.dvds.dvd.titres.fr) {
            dvds.push({
                dvdfr: frdvds.dvds.dvd.id,
                ean,
                title: frdvds.dvds.dvd.titres.fr,
                year: frdvds.dvds.dvd.annee,
                cover: frdvds.dvds.dvd.cover,
            });
        }
        if(frdvds.dvds.dvd.titres.vo) {
            dvds.push({
                dvdfr: frdvds.dvds.dvd.id,
                ean,
                title: frdvds.dvds.dvd.titres.vo,
                year: frdvds.dvds.dvd.annee,
                cover: frdvds.dvds.dvd.cover,
            });
        }
        if(frdvds.dvds.dvd.titres.alternatif) {
            dvds.push({
                dvdfr: frdvds.dvds.dvd.id,
                ean,
                title: frdvds.dvds.dvd.titres.alternatif,
                year: frdvds.dvds.dvd.annee,
                cover: frdvds.dvds.dvd.cover,
            });
        }
    }

    res.send(JSON.stringify(dvds));
});

const invoker = new gcp.cloudfunctions.FunctionIamMember("invoker", {
    project: dvdfr.function.project,
    region: dvdfr.function.region,
    cloudFunction: dvdfr.function.name,
    role: "roles/cloudfunctions.invoker",
    member: "allUsers",
});